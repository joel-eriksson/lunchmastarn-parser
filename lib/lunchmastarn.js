const moment = require("moment");
const { JSDOM } = require("jsdom");
const request = require("request-promise-native");

module.exports = () => {
    const rest = "13537"; // Lunchmästar'n

    return new Promise((resolve, reject) => {
        return request({
            url: `http://kvartersmenyn.se/kvmapi/rest/${rest}`,
            json: true,
        }).then((data) => {
            const apiMenuHtml = data[0].menu;
            const dom = new JSDOM(apiMenuHtml, {
                includeNodeLocations: true,
            });

            const window = dom.window;
            const document = window.document;

            let menuHtml;
            const days = [
                "monday",
                "tuesday",
                "wednesday",
                "thursday",
                "friday",
            ];

            const daysSwedish = [
                "Måndag",
                "Tisdag",
                "Onsdag",
                "Torsdag",
                "Fredag",
            ];

            let daysMeal = {
                monday: {
                    date: 0,
                    meals: []
                },
                tuesday: {
                    date: 0,
                    meals: []
                },
                wednesday: {
                    date: 0,
                    meals: []
                },
                thursday: {
                    date: 0,
                    meals: []   
                },
                friday: {
                    date: 0,
                    meals: []
                },
            }

            document.querySelectorAll("div").forEach((tag) => {
                if (menuHtml) {
                    return;
                }

                if (
                    tag.textContent.includes("Måndag")
                    && tag.textContent.includes("Tisdag")
                    && tag.textContent.includes("Onsdag")
                    && tag.textContent.includes("Torsdag")
                    && tag.textContent.includes("Fredag")
                ) {
                    menuHtml = tag.innerHTML;

                    tag.querySelectorAll("br").forEach((brElement) => {
                        brElement.remove();
                    });

                    tag.querySelectorAll("strong").forEach((strongElement) => {
                        daysSwedish.forEach((day, i) => {
                            if (strongElement.textContent !== day) {
                                return;
                            }

                            daysMeal[days[i]].date = parseInt(moment().days(days[i]).format("x"));
                            let prevElement = strongElement.nextSibling;
                            while (prevElement) {
                                if (prevElement.nodeName === "#text") {
                                    daysMeal[days[i]].meals.push(prevElement.textContent);
                                    prevElement = prevElement.nextSibling;
                                } else if (prevElement.nodeName === "br") {
                                    prevElement = prevElement.nextSibling;
                                } else {
                                    break;
                                }
                            }
                        });
                    });

                    menuHtml = tag.innerHTML;
                }
            });

            if (!menuHtml) {
                return reject(new Error("Could not find target HTML"));
            }

            let output = {
                name: data[0].name,
                logo: data[0].logo,
                days: daysMeal,
            };

            return resolve(output);
        }).catch((error) => {
            return reject(error);
        });
    });
    
};
